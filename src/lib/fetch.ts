import merge from 'lodash/merge';
import get from 'lodash/get';
import set from 'lodash/set';

import helpers from '../utils/helpers';

// eslint-disable-next-line @typescript-eslint/no-explicit-any
type Obj = { [key: string]: any };

export interface RequestError {
  message: string;
  status: number;
}

export class ResponseError extends Error {
  status?: number;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  data?: any;

  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  body?: any;
}

export interface RequestData extends RequestInit {
  errors?: RequestError[];
  query?: Obj;
  params?: Obj;
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  body?: any;
  customContentType?: boolean;
}

function serialize(query: Obj): string {
  const queries = Object.keys(query).reduce((accum: string[], key) => {
    const value = query[key];

    if (value != null && !helpers.isArray(value) && !helpers.isObject(value)) {
      const item = `${encodeURIComponent(key)}=${encodeURIComponent(value)}`;

      accum.push(item);
    }

    if (helpers.isArray(value)) {
      const array = (value as Array<string | number | boolean>);

      array.forEach((arrayValue) => {
        const item = `${encodeURIComponent(key)}=${encodeURIComponent(arrayValue)}`;

        accum.push(item);
      });
    }

    return accum;
  }, []);

  return `?${queries.join('&')}`;
}

function setSession(data: Obj): Obj {
  const token = get(data, 'headers.authorization', '') || sessionStorage.getItem('access_token') || localStorage.getItem('access_token');

  if (token) { set(data, 'headers.authorization', token); }

  return data;
}

async function customFetch(path: string, data: RequestData = {}) {
  const {
    errors = [],
    query,
    params,
    customContentType,
    ...dataToFetch
  } = data;

  let pathToServer = path;

  setSession(dataToFetch);

  if (dataToFetch.method && !['GET', 'DELETE'].includes(dataToFetch.method) && !customContentType) {
    const haveContentType = get(dataToFetch, 'headers.Content-Type', '');

    if (!haveContentType) { merge(dataToFetch, { headers: { 'Content-Type': 'application/json' } }); }

    const cleanObject = helpers.omitByDeep(dataToFetch.body, (value: unknown) => !!value);

    set(dataToFetch, 'body', JSON.stringify(cleanObject));
  }

  if (params) {
    Object.keys(params).forEach((key) => {
      pathToServer = pathToServer.replace(`:${key}`, params[key]);
    });
  }

  if (query) { pathToServer = `${pathToServer}${serialize(query)}`; }

  let response = new Response();

  try {
    response = await fetch(`${pathToServer}`, dataToFetch);
  } catch ({ message }) {
    const error = new ResponseError('Error de conexión, verifica tu  conexión a internet, o contacta a soporte');

    error.body = { originalMessage: message };

    throw error;
  }

  if (!response.ok) {
    let errorMessage = '';

    // eslint-disable-next-line @typescript-eslint/no-explicit-any
    let payload: any = {};

    const customError = errors.find(error => error.status === response.status);
    if (customError) { errorMessage = customError.message; }

    const contentType = response.headers.get('Content-Type');

    if (contentType?.includes('application/json')) {
      payload = await response.json();

      if (!errorMessage) { errorMessage = payload.message; }
    }

    if (!errorMessage) { errorMessage = 'Ocurrió un error al tratar de consultar el servidor, consulte a soporte.'; }

    const errorObject = new ResponseError(errorMessage);

    errorObject.body = payload;
    errorObject.status = response.status;

    throw errorObject;
  }

  return response;
}

const methodGenerator = (method: string) => async (path: string, options: RequestData) => {
  const response = await customFetch(path, { method, ...options });

  const contentType = response.headers.get('Content-Type');

  if (contentType?.includes('application/json')) {
    const payload = await response.json();

    return payload;
  }

  return response;
};

customFetch.get = methodGenerator('GET');
customFetch.post = methodGenerator('POST');
customFetch.patch = methodGenerator('PATCH');
customFetch.put = methodGenerator('PUT');
customFetch.del = methodGenerator('DELETE');

export default customFetch;
