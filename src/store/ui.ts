import {
  Module,
  VuexModule,
  Mutation,
  Action,
} from 'vuex-module-decorators';

import helpers from '../utils/helpers';

const FIRST_POSITION = 0;
const NOTIFICATIONS_QUANTITY = 5;

// loading-animation-duration
const LOADING_ANIMATION_TIMING = 1450 / 2;
const LOADING_ANIMATION_HIDE_TIMING = 400 / 2;
const NOTIFICATION_DELAY = 5000;

export interface Notification {
  content: string;
  type: string;
  datetime?: Date;
  duration?: number;
}

@Module({
  name: 'ui',
  stateFactory: true,
  namespaced: true,
})
class UIStore extends VuexModule {
  navbar = true;

  loading = false;

  closing = false;

  mobile = false;

  notifications: Notification[] = [];

  get lastFiveNotifications() {
    const clone = this.notifications.slice(FIRST_POSITION).reverse();

    return clone.slice(FIRST_POSITION, NOTIFICATIONS_QUANTITY);
  }

  @Mutation
  setInitialState() {
    this.navbar = true;
    this.loading = false;
    this.closing = false;
    this.mobile = false;
    this.notifications = [];
  }

  @Mutation
  toggleNavbar() {
    this.navbar = !this.navbar;
  }

  @Mutation
  toggleLoading() {
    this.loading = !this.loading;
  }

  @Mutation
  toggleCLosing() {
    this.closing = !this.closing;
  }

  @Mutation
  toggleMobile() {
    this.mobile = !this.mobile;
  }

  @Mutation
  setLoading(value: boolean) {
    this.loading = value;
  }

  @Mutation
  setNavbar(value: boolean) {
    this.navbar = value;
  }

  @Mutation
  setClosing(value: boolean) {
    this.closing = value;
  }

  @Mutation
  addNotification(payload: Notification) {
    const notification = payload;

    if (!notification.datetime) { notification.datetime = new Date(); }

    this.notifications.push(notification);
  }

  @Mutation
  deleteNotification(payload: Date) {
    const notifications = this.notifications.filter(
      notification => notification.datetime !== payload,
    );

    this.notifications = notifications;
  }

  @Mutation
  closeLoading() {
    this.loading = false;
    this.closing = false;
  }

  @Action({ commit: 'closeLoading' })
  async initLoading() {
    this.context.commit('setLoading', true);

    await helpers.wait(LOADING_ANIMATION_TIMING);

    this.context.commit('setClosing', true);

    await helpers.wait(LOADING_ANIMATION_HIDE_TIMING);
  }

  @Action({ commit: 'deleteNotification' })
  async addTemporalNotification(payload: Notification) {
    const { duration = NOTIFICATION_DELAY, ...notification } = payload;

    const now = new Date();

    notification.datetime = now;

    this.context.commit('addNotification', notification);

    await helpers.wait(duration);

    return now;
  }
}

export default UIStore;
