package api

import (
	"fmt"
	"math/rand"
	"net/http"
	"strconv"
	"time"

	svg "github.com/ajstarks/svgo"
)

// workingData data for working in stars handler
type workingData struct {
	ws string
	hs string

	mis string
	mas string

	miss string
	mass string

	wi int
	hi int

	mini int
	maxi int

	mins int
	maxs int

	misi int
	masi int

	colors []string
}

func getWorkData(r *http.Request) workingData {
	ws := r.FormValue("width")
	hs := r.FormValue("height")

	mis := r.FormValue("minItems")
	mas := r.FormValue("maxItems")

	miss := r.FormValue("minSize")
	mass := r.FormValue("maxSize")

	colors := r.Form["colors"]

	if ws == "" {
		ws = "0"
	}

	if hs == "" {
		hs = "0"
	}

	if mis == "" {
		mis = "0"
	}

	if mas == "" {
		mas = "0"
	}

	if miss == "" {
		miss = "0"
	}

	if mass == "" {
		mass = "0"
	}

	if len(colors) == 0 {
		colors = []string{"white"}
	}

	return workingData{
		ws: ws,
		hs: hs,

		mis: mis,
		mas: mas,

		miss: miss,
		mass: mass,

		colors: colors,
	}
}

func (data workingData) parse() workingData {
	wi, werr := strconv.Atoi(data.ws)

	if werr != nil {
		wi = 0
	}

	hi, herr := strconv.Atoi(data.hs)

	if herr != nil {
		hi = 0
	}

	mini, minierr := strconv.Atoi(data.mis)

	if minierr != nil {
		mini = 0
	}

	maxi, maxierr := strconv.Atoi(data.mas)

	if maxierr != nil {
		maxi = 0
	}

	misi, misierr := strconv.Atoi(data.miss)

	if misierr != nil {
		misi = 0
	}

	masi, masierr := strconv.Atoi(data.mass)

	if masierr != nil {
		masi = 0
	}

	// max number of pixels in width
	if wi > 10000 {
		wi = 10000
	}

	// max number of pixels in height
	if hi > 10000 {
		hi = 10000
	}

	// min number stars
	if mini < 0 {
		mini = 0
	}

	// max number of stars
	if maxi > 8000 {
		maxi = 8000
	}

	// min size of the stars
	if misi < 1 {
		misi = 1
	}

	// max size of the stars
	if masi > 6 {
		masi = 6
	}

	data.wi = wi
	data.hi = hi

	data.mini = mini
	data.maxi = maxi

	data.misi = misi
	data.masi = masi

	return data
}

// StarsHandler HTTP handler for vercel serverless
func StarsHandler(w http.ResponseWriter, r *http.Request) {
	rand.Seed(time.Now().UnixNano())
	r.ParseForm()

	dp := getWorkData(r).parse()

	// Min duration
	// md := 1800
	// Max duration
	// mad := 4000

	q := rand.Intn(dp.maxi-dp.mini+1) + dp.mini
	cl := len(dp.colors)

	w.Header().Set("Content-Type", "image/svg+xml")

	sv := svg.New(w)

	sv.Start(dp.wi, dp.hi)

	for i := 0; i < q; i++ {
		c := "white"

		if cl > 1 {
			ci := rand.Intn(cl)
			c = dp.colors[ci]
		}

		si := rand.Intn(dp.masi-dp.misi+1) + dp.misi
		cx := rand.Intn(dp.wi + 1)
		cy := rand.Intn(dp.hi + 1)
		// dur := rand.Intn(mad-md+1) + md

		sv.Circle(cx, cy, si, fmt.Sprintf("fill:%s;", c))
	}

	sv.End()
}
