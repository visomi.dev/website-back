import set from 'lodash/set';
import { Configuration } from '@nuxt/types';

const config: Configuration = {
  server: {
    port: process.env.PORT || 4000,
  },
  router: {
    linkExactActiveClass: 'is-active',
  },
  srcDir: 'src',
  head: {
    title: 'visomi.dev | Software Architect',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      {
        hid: 'description',
        name: 'description',
        content: 'visomi.dev landing page',
      },
    ],
    link: [{
      rel: 'icon',
      type: 'image/x-icon',
      href: '/favicon.png',
    }],
  },
  css: [
    '~/assets/styles/index.sass',
  ],
  buildModules: ['@nuxt/typescript-build'],
  build: {
    extend(conf: { isDev: boolean }, ctx: { isDev: any; isClient: any; }) {
      if (ctx.isDev) {
        set(conf, 'devtool', ctx.isClient ? 'source-map' : 'inline-source-map');
      }
    },
  },
  typescript: {
    typeCheck: {
      eslint: {
        files: './**/*.{ts,vue}',
      },
    },
  },
  target: 'static',
};

export default config;
